const burgerButton = document.querySelector(".burger-button");
const headerNav = document.querySelector(".header-nav");
burgerButton.addEventListener("click", () => {
  burgerButton.classList.toggle("burger-button--active");
  headerNav.classList.toggle("header-nav--active");

});
const listener = document.querySelector(".header-nav__list");
listener.children[0].children[0].classList.add("header-nav__link--active");
listener.addEventListener("click", (e) => {
  for (const iterator of listener.children) {
    iterator.children[0].classList.remove("header-nav__link--active");
  }
  e.target.closest("li").children[0].classList.add("header-nav__link--active");
});

const children =document.querySelector(".instagram-shot__list")
console.log(children.children[1]);