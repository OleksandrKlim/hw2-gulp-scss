// let project_folder = "dist";
// let source_folder = "@src";
//  let path = {
//      build:{
//          html:project_folder+"/",
//          css:project_folder+"/css/",
//          js:project_folder+"/js/",
//          img:project_folder+"/img/",
//      },
//      src:{
//          html:source_folder+"/*.html",
//          css:source_folder+"/scss/style.scss",
//          js:source_folder+"/js/main.js",
//          img:source_folder+"/img/**/*.{jpg,png,svg,gif,ico,webp}",
//      },
//      watch:{
//          html:source_folder+"/**/*.html",
//          css:source_folder+"/scss/**/*.scss",
//          js:source_folder+"/js/**/*.js",
//          img:source_folder+"/img/**/*.{jpg,png,svg,gif,ico,webp}",
//      },
//      clean: "./"+ project_folder + "/"
//  }
//  let {src, dest} = require("gulp"),
//  gulp = require("gulp");
//  browsersync = require("gulp");
//  browsersync = require("browser-sync").create();
//
//  function browserSync (params){
// browsersync.init({
//     server:{
//         baseDir: "./"+ project_folder + "/"
//     },
//     port: 3000,
//     notify:false,
// })
//      function html() {
//          return src(path.src.html)
//              .pipe(dest(path.build.html))
//              .pipe(browserSync.stream())
//      }
//  }
//  let build = gulp.series(html);
//  let watch = gulp.parallel(build,browserSync);
//  exports.html = html;
//  exports.build = build;
//  exports.watch = watch;
//  exports.default = watch;

// const {series} = require("gulp");
// const task = (done) => {
//     console.log("hi");
//     done();
// }
// exports.default = series(task);

const {series, parallel, src, dest, watch} = require('gulp')
const browserSync = require('browser-sync').create()
const sass = require('gulp-sass')(require('sass'))

const serv = () => {
	browserSync.init({
		server: {
			baseDir: './',
		},
		open: true,
	})
}

const scripts = done => {
	src('./src/js/main.js').pipe(dest('./dist/js')).pipe(browserSync.stream())
	done()
}
const styles = done => {
	src('./src/scss/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(dest('./dist/css'))
		.pipe(browserSync.stream())
	done()
}

const images = () => {
	src('./src/img/**/*.{jpg,jpeg,png,svg}').pipe(dest('./dist/img'))
}

const watcher = () => {
	watch('*.html').on('change', browserSync.reload)
	watch('./src/js/*.js').on('change', series(scripts, browserSync.reload))
	watch('./src/scss/**/*.scss', styles)
	watch('./src/img/**/*.{jpg,jpeg,png,svg}').on(
		'change',
		series(images, browserSync.reload)
	)
}
exports.default = parallel(serv, watcher, series(styles, scripts, images))
